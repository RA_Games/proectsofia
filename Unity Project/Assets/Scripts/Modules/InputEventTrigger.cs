﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputEventTrigger : MonoBehaviour {

    public UnityEvent onPressed;

    [SerializeField] MyController controller;

    private enum Buttons
    {
        SUBMIT, AXIS_RIGHT, AXIS_DOWN, AXIS_LEFT, AXIS_UP
    }

    [SerializeField] Buttons button;

    private void Update()
    {
        switch (button)
        {
            case Buttons.SUBMIT:
                if (controller.Submit().GetKeyDown())
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_RIGHT:
                if (controller.GetAxis().x >= 0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_DOWN:
                if (controller.GetAxis().y <= -0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_LEFT:
                if (controller.GetAxis().x <= -0.1f)
                {
                    onPressed.Invoke();
                }
                break;
            case Buttons.AXIS_UP:
                if (controller.GetAxis().y >= 0.1f)
                {
                    onPressed.Invoke();
                }
                break;
        }
    }

}
