﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float speed = 1;

	void LateUpdate () {

        Vector3 end = Vector3.zero;

        if (target != null)
            end = target.position;

        end.z = transform.position.z;

        transform.position = Vector3.Lerp(transform.position, end, Time.deltaTime * speed);

	}
}
