﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class TV : MonoBehaviour {

    private Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }

    public void SwitchOn()
    {
        _anim.SetBool("IsOn", true);
    }

    public void SwitchOff()
    {
        _anim.SetBool("IsOn", true);
    }
}
