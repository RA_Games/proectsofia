﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class QuickTimeEvents : MonoBehaviour {

    public UnityEvent finishEvent;
    public float progress = 100;

    public void FinishQuickTimeEvent()
    {
        finishEvent.Invoke();
        this.enabled = false;
    }
}
