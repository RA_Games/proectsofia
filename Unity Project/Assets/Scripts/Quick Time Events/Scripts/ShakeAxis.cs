﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeAxis : QuickTimeEvents {

    public MyController controller;

    public GameEvent minEvent;
    public GameEvent maxEvent;

    public float maxLife = 100;
    public float damage = 10;
    public float regenerationPerSecond = 10;

    private bool isOnMin;

    IEnumerator Start()
    {
        while (progress > 0)
        {
            yield return new WaitForSeconds(1);
            progress += 10;

            progress = Mathf.Clamp(progress, 0, maxLife);
        }
    }

    public void Update()
    {
        if (progress > 0)
        {
            if (!isOnMin)
            {
                if (controller.GetAxis().x <= -0.1f)
                {
                    isOnMin = true;
                    minEvent.Raise();
                    progress -= damage;
                    progress = Mathf.Clamp(progress, 0, maxLife);
                }
            }
            else
            {
                if (controller.GetAxis().x >= 0.1f)
                {
                    isOnMin = false;
                    maxEvent.Raise();
                    progress -= damage;
                    progress = Mathf.Clamp(progress, 0, maxLife);
                }
            }
        }
        else
        {
            progress = 0;
            StopAllCoroutines();
            FinishQuickTimeEvent();
        }
    }
}
