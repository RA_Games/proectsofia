﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Legs : MonoBehaviour {

    [SerializeField]
    private MyController controller;

    [SerializeField]
    private Animator anim;

    public float speed = 1;
    public float maxSpeed = 5;

    private Rigidbody2D _rb;

	void Start ()
    {
        _rb = GetComponent<Rigidbody2D>();
	}
	
	void Update ()
    {
        Vector2 direction = controller.GetAxis();

        if (_rb.velocity.magnitude < maxSpeed)
            _rb.velocity += direction * Time.deltaTime * speed;

        anim.SetFloat("Speed", _rb.velocity.normalized.magnitude);

        if (_rb.velocity.x > 0.1f)
            transform.rotation = Quaternion.Euler(0,0,0);

        if (_rb.velocity.x < -0.1f)
            transform.rotation = Quaternion.Euler(0, 180, 0);
    }
}
