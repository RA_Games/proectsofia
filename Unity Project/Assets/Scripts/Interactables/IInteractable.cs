﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {

    bool IsLocked { get; }

    void Interact();

    void Lock();
    void Unlock();
}
