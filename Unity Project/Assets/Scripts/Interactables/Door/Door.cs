﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour ,IInteractable
{
    public bool isLocked;
    public bool isOpen;

    [SerializeField]
    private Animator anim;

    public bool IsLocked
    {
        get
        {
            return isLocked;
        }
    }

    public void Interact()
    {
        if(!isLocked){
            isOpen = !isOpen;
            anim.SetBool("IsOpen", isOpen);
        }
    }

    public void Lock()
    {
        isLocked = true;
    }

    public void Unlock()
    {
        isLocked = false;
    }

    private void OnValidate()
    {
        anim.SetBool("IsOpen", isOpen);
    }
}
