﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

    [SerializeField] MyController controller;

    [SerializeField] CharacterController2D characterController;
    [SerializeField] Animator animator;

    private float _horizontalMovement;

    public float speed = 1f;
    private bool _isJump;
    private bool _isCrouch;

    void Update ()
    {
        _horizontalMovement = controller.GetHorizontalAxis().GetAxisRaw() * speed;
        animator.SetFloat("Speed", Mathf.Abs(_horizontalMovement));
        animator.SetBool("IsGrounded", characterController.IsGrounded());

        if (controller.Submit().GetKeyDown())
        {
            _isJump = true;
        }
	}

    private void FixedUpdate()
    {
        characterController.Move(_horizontalMovement * Time.fixedDeltaTime, _isCrouch, _isJump);

        _isJump = false;
    }
}
