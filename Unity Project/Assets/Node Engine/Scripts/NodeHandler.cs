﻿using UnityEngine;

public class NodeHandler : MonoBehaviour
{
    private static NodeHandler _singleton;

    public static NodeHandler Singleton
    {
        get
        {
            if(_singleton == null)
                _singleton = new GameObject("NodeWaitHandler", typeof(NodeHandler)).GetComponent<NodeHandler>();

            return _singleton;
        }
    }

}
