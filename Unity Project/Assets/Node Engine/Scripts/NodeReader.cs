﻿using UnityEngine;

public class NodeReader : MonoBehaviour {

    public NodeBehaviour nodeBehaviour;
    public bool startOnAwake = true;
    private Node current;

    IntegerValue value;
    DoubleValue Dvalue;

    private void Start()
    {
        if (startOnAwake)
        {
            StartRead();
        }
    }

    public void SkipNode()
    {
        current.CancelRead();
        Continue();
    }

    public void StartRead()
    {
        current = nodeBehaviour.GetStartNode();
        current.ReadNode(Continue);
    }

    private void Continue()
    {
        current = current.nextNode.GetNode(nodeBehaviour);

        if (current != null)
        {
            current.ReadNode(Continue);
        }
    }
}
