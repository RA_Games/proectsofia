﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class NodeDialogue : Node
{
    public string header;
    public string text;

    public bool useDialogueSheet;
    public bool useRangue;
    public string dialogueID;
    public float rangueMinValue, rangueMaxValue;
    public DialogueSheet dialogueSheet;
    public DialogueSheet.Languages previewLanguage = DialogueSheet.Languages.ENGLISH;
    public DialogueType dialogueType;
    public Actor actor;

    public enum DialogueType
    {
        SCREEN, BUBBLE
    }

    public NodeDialogue(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }

    public override void CancelRead()
    {
        
    }

    protected override void Read()
    {
        if (dialogueType == DialogueType.SCREEN)
        {
            if (useDialogueSheet)
            {
                string dialogueText = dialogueSheet.Get(dialogueID).GetDialogue(DialogueSheet.Languages.ENGLISH); //Change to global language.
                DependencyInyector.Singleton.GetDependency<NovelGraphicDependency>().GetComponent<ScreenDialogueHandler>().Write(header, dialogueText, FinishRead);
            }
            else
            {
                DependencyInyector.Singleton.GetDependency<NovelGraphicDependency>().GetComponent<ScreenDialogueHandler>().Write(header, text, FinishRead);
            }
        }else if (dialogueType == DialogueType.BUBBLE)
        {
            foreach (ChatBubble bubbles in GameObject.FindObjectsOfType<ChatBubble>())
            {
                if(actor == bubbles.actor)
                {
                    if (useDialogueSheet)
                    {
                        if (!useRangue)
                        {
                            string dialogueText = dialogueSheet.Get(dialogueID).GetDialogue(DialogueSheet.Languages.ENGLISH); //Change to global language.
                            ChatBubble.GetBubbleInScene(actor).Write(dialogueText, FinishRead);
                        }
                        else
                        {
                            int count = (int)(rangueMaxValue - rangueMinValue);
                            string[] dialogues = new string[count + 1];
                            for (int i = 0; i <= count; i++)
                            {
                                int index = (int)rangueMinValue + i;
                                dialogues[i] = dialogueSheet.Get(index).GetDialogue(DialogueSheet.Languages.ENGLISH);
                            }

                            ChatBubble.GetBubbleInScene(actor).Write(dialogues, FinishRead);
                        }
                    }
                    else
                    {
                        ChatBubble.GetBubbleInScene(actor).Write(text, FinishRead);
                    }

                    return;
                }
            }
        }
    }
}
