﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class NodeAddReader : Node
{
    public NodeBehaviour nextNodeBehaviour;

    public NodeAddReader(Vector2 position, float width, float height, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<ConnectionPoint> OnClickInPoint, Action<ConnectionPoint> OnClickOutPoint, Action<Node> OnClickRemoveNode) : base(position, width, height, nodeStyle, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode)
    {
    }

    public override void CancelRead()
    {
        
    }

    protected override void Read()
    {
        NodeReader reader = new GameObject("Node Reader", typeof(NodeReader)).GetComponent<NodeReader>();
        reader.nodeBehaviour = nextNodeBehaviour;
        FinishRead();
    }
}
