﻿using UnityEngine;
using UnityEditor;

public class NodeBasedEditor : EditorWindow
{
    private GUIStyle nodeStyle;
    private GUIStyle selectedNodeStyle;
    private GUIStyle inPointStyle;
    private GUIStyle outPointStyle;

    private ConnectionPoint selectedInPoint;
    private ConnectionPoint selectedOutPoint;

    public NodeBehaviour nodeBehaviour;

    private Vector2 offset;
    private Vector2 drag;

    [MenuItem("Window/Node Based Editor")]
    private static void OpenWindow()
    {
        NodeBasedEditor window = GetWindow<NodeBasedEditor>();
        window.titleContent = new GUIContent("Node Based Editor");
    }

    private void OnFocus()
    {
        Rebuild();
    }

    private void OnSelectionChange()
    {
        if (Selection.activeObject is NodeBehaviour)
        {
            if (Selection.assetGUIDs.Length > 0)
            {
                nodeBehaviour = Selection.activeObject as NodeBehaviour;
                Repaint();
                return;
            }
        }

        nodeBehaviour = null;

        Repaint();
    }

    private void Rebuild()
    {
        if (nodeBehaviour != null)
        {
            foreach (Node node in nodeBehaviour.nodes)
            {
                node.Rebuild(OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);
            }
        }
    }

    private void OnEnable()
    {
        nodeStyle = new GUIStyle();
        nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
        nodeStyle.border = new RectOffset(12, 12, 12, 12);

        selectedNodeStyle = new GUIStyle();
        selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
        selectedNodeStyle.border = new RectOffset(12, 12, 12, 12);

        inPointStyle = new GUIStyle();
        inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
        inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
        inPointStyle.border = new RectOffset(4, 4, 12, 12);

        outPointStyle = new GUIStyle();
        outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
        outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
        outPointStyle.border = new RectOffset(4, 4, 12, 12);
    }

    private void OnGUI()
    {
        DrawGrid(20, 0.2f, Color.gray);
        DrawGrid(100, 0.4f, Color.gray);

        if (nodeBehaviour != null)
        {
            DrawNodes();

            DrawConnectionLine(Event.current);
            DrawConnections();
            ProcessNodeEvents(Event.current);
            ProcessEvents(Event.current);
        }

        if (GUI.changed) Repaint();
    }

    private void DrawConnections()
    {
        foreach (Node n in nodeBehaviour.nodes)
        {
            if (n.nextNode.GetNode(nodeBehaviour) != null)
            {
                Handles.DrawBezier(
                   n.nextNode.GetNode(nodeBehaviour).inPoint.rect.center,
                   n.outPoint.rect.center,
                   n.nextNode.GetNode(nodeBehaviour).inPoint.rect.center + Vector2.left * 50f,
                   n.outPoint.rect.center - Vector2.left * 50f,
                   Color.white,
                   null,
                   2f
               );

                if (Handles.Button((n.nextNode.GetNode(nodeBehaviour).inPoint.rect.center + n.outPoint.rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
                {
                    n.nextNode.UnsetNextNode();
                }
            }
        }
    }

    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for (int i = 0; i < widthDivs; i++)
        {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for (int j = 0; j < heightDivs; j++)
        {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    private void DrawNodes()
    {
        if (nodeBehaviour.nodes != null)
        {
            for (int i = 0; i < nodeBehaviour.nodes.Count; i++)
            {
                nodeBehaviour.nodes[i].Draw();
            }
        }
    }

    private void ProcessEvents(Event e)
    {
        drag = Vector2.zero;

        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                { 
                    ClearConnectionSelection();
                }

                if (e.button == 1)
                {
                    ProcessContextMenu(e.mousePosition);

                    foreach (Node n in nodeBehaviour.nodes)
                    {
                        if (n.rect.Contains(e.mousePosition) && n.isSelected)
                        {
                            ProcessNodeContextMenu(n);
                            e.Use();
                            break;
                        }
                    }
                }
                break;

            case EventType.MouseDrag:
                if (e.button == 0)
                {
                    OnDrag(e.delta);
                }
                break;
        }
    }

    private void ProcessNodeEvents(Event e)
    {
        if (nodeBehaviour.nodes != null)
        {
            for (int i = nodeBehaviour.nodes.Count - 1; i >= 0; i--)
            {
                bool guiChanged = nodeBehaviour.nodes[i].ProcessEvents(e);

                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
    }

    private void DrawConnectionLine(Event e)
    {
        if (selectedInPoint != null && selectedOutPoint == null)
        {
            Handles.DrawBezier(
                selectedInPoint.rect.center,
                e.mousePosition,
                selectedInPoint.rect.center + Vector2.left * 50f,
                e.mousePosition - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        if (selectedOutPoint != null && selectedInPoint == null)
        {
            Handles.DrawBezier(
                selectedOutPoint.rect.center,
                e.mousePosition,
                selectedOutPoint.rect.center - Vector2.left * 50f,
                e.mousePosition + Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }
    }

    private void ProcessContextMenu(Vector2 mousePosition)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Basic/Add Event Trigger Node"), false, () => OnClickTriggerEventNode(mousePosition));
        genericMenu.AddItem(new GUIContent("Basic/Add Event Listen Node"), false, () => OnClickListenEventNode(mousePosition));
        genericMenu.AddItem(new GUIContent("Basic/Add Wait Node"), false, () => OnClickWaitForSecondsNode(mousePosition));
        genericMenu.AddItem(new GUIContent("Basic/Add End Node"), false, () => OnClickEndNode(mousePosition));
        genericMenu.AddItem(new GUIContent("Basic/Add Reader Node"), false, () => OnClickReaderNode(mousePosition));
        genericMenu.AddItem(new GUIContent("Novel/Dialogue"), false, () => OnClickDialogueNode(mousePosition));
        genericMenu.ShowAsContext(); 
    }

    private void ProcessNodeContextMenu(Node node)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Remove node"), false, node.OnClickRemoveNode);
        genericMenu.ShowAsContext();
    }

    private void OnDrag(Vector2 delta)
    {
        drag = delta;

        if (nodeBehaviour.nodes != null)
        {
            for (int i = 0; i < nodeBehaviour.nodes.Count; i++)
            {
                nodeBehaviour.nodes[i].Drag(delta);
            }
        }

        GUI.changed = true;
    }

    private void OnClickTriggerEventNode(Vector2 mousePosition)
    {
        Node n = new NodeTriggerEvent(mousePosition, 200, 50, nodeStyle, selectedNodeStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private void OnClickEndNode(Vector2 mousePosition)
    {
        GUIStyle style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node4.png") as Texture2D;
        style.border = new RectOffset(12, 12, 12, 12);

        GUIStyle selectedStyle = new GUIStyle();
        selectedStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node4 on.png") as Texture2D;
        selectedStyle.border = new RectOffset(12, 12, 12, 12);

        Node n = new NodeEnd(mousePosition, 200, 50, style, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private void OnClickDialogueNode(Vector2 mousePosition)
    {
        GUIStyle style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node6.png") as Texture2D;
        style.border = new RectOffset(12, 12, 12, 12);

        GUIStyle selectedStyle = new GUIStyle();
        selectedStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node6 on.png") as Texture2D;
        selectedStyle.border = new RectOffset(12, 12, 12, 12);

        Node n = new NodeDialogue(mousePosition, 200, 50, style, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private void OnClickReaderNode(Vector2 mousePosition)
    {
        GUIStyle style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node5.png") as Texture2D;
        style.border = new RectOffset(12, 12, 12, 12);

        GUIStyle selectedStyle = new GUIStyle();
        selectedStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node5 on.png") as Texture2D;
        selectedStyle.border = new RectOffset(12, 12, 12, 12);

        Node n = new NodeAddReader(mousePosition, 200, 50, style, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private void OnClickListenEventNode(Vector2 mousePosition)
    {
        GUIStyle style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node2.png") as Texture2D;
        style.border = new RectOffset(12, 12, 12, 12);

        GUIStyle selectedStyle = new GUIStyle();
        selectedStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node2 on.png") as Texture2D;
        selectedStyle.border = new RectOffset(12, 12, 12, 12);

        Node n = new NodeListenGameEvent(mousePosition, 200, 50, style, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private void OnClickWaitForSecondsNode(Vector2 mousePosition)
    {

        GUIStyle style = new GUIStyle();
        style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node3.png") as Texture2D;
        style.border = new RectOffset(12, 12, 12, 12);

        GUIStyle selectedStyle = new GUIStyle();
        selectedStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node3 on.png") as Texture2D;
        selectedStyle.border = new RectOffset(12, 12, 12, 12);

        Node n = new NodeWaitForSeconds(mousePosition, 200, 50, style, selectedStyle, inPointStyle, outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode);

        n.nodeId = RenameNode(n);

        nodeBehaviour.nodes.Add(n);

        Repaint();
    }

    private string RenameNode(Node newNode)
    {
        bool isFound = false;
        string nodeId = newNode.nodeId;

        foreach (Node node in nodeBehaviour.nodes)
        {
            if (nodeId == node.nodeId)
            {
                isFound = true;
                break;
            }
        }

        if (!isFound)
            return nodeId;

        isFound = false;

        for (int i = 0; i < 500; i++)
        {
            isFound = false;
            nodeId = newNode.nodeId + " " + i;

            foreach (Node node in nodeBehaviour.nodes)
            {
                if (nodeId == node.nodeId)
                {
                    isFound = true;
                    break;
                }
            }

            if (!isFound)
                return nodeId;
        }

        return nodeId;
    }

    private void OnClickInPoint(ConnectionPoint inPoint)
    {
        selectedInPoint = inPoint;

        CreateConnection();
    }

    private void OnClickOutPoint(ConnectionPoint outPoint)
    {
        selectedOutPoint = outPoint;

        if (selectedInPoint != null)
        {
            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection();
                ClearConnectionSelection();
            }
            else
            {
                ClearConnectionSelection();
            }
        }
    }

    private void OnClickRemoveNode(Node node)
    {
        nodeBehaviour.nodes.Remove(node);
    }

    private void CreateConnection()
    {
        if (selectedInPoint != null)
        {
            if (selectedOutPoint != null)
            {
                if (selectedOutPoint.node != selectedInPoint.node)
                {
                    selectedOutPoint.node.nextNode.SetNextNode(selectedInPoint.node.nodeId);
                }
            }
        }

        Rebuild();
        ClearConnectionSelection();
    }

    private void ClearConnectionSelection()
    {
        selectedInPoint = null;
        selectedOutPoint = null;
    }
}