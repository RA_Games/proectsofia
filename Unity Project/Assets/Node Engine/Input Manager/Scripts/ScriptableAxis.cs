﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/Scriptable Axis")]
public class ScriptableAxis : ScriptableInput{

    public string inputName;

    public float GetAxis()
    {
        return Input.GetAxis(inputName);
    }

    public float GetAxisRaw()
    {
        return Input.GetAxisRaw(inputName);
    }
}
