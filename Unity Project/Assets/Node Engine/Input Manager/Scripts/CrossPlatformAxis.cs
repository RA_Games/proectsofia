﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/CrossPlatform Axis")]
public class CrossPlatformAxis : ScriptableObject
{
    [SerializeField]
    private ControllerType mode;

    [SerializeField]
    private List<ScriptableAxis> allowedAxis;

    public ScriptableAxis Get()
    {
        foreach (ScriptableAxis key in allowedAxis)
        {
            if (key.controllerType == mode)
                return key;
        }

        Debug.Log("Axis not allowed for " + mode);
        return null;
    }

    public ScriptableAxis Get(ControllerType mode)
    {
        foreach (ScriptableAxis key in allowedAxis)
        {
            if (key.controllerType == mode)
                return key;
        }

        Debug.Log("Axis not allowed for " + mode);
        return null;
    }
}
