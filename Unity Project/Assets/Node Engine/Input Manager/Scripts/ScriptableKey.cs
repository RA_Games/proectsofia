﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/Scriptable Key")]
public class ScriptableKey : ScriptableInput {

    public PcButtons pc_butons;

    public XboxButtons xbox_buttons;

    public PlaystationButtons play_buttons;

    public SwitchButtons switch_buttons;

    public MouseButtons mouse_buttons;

    public KeyCode keyCode;

    public bool GetKey()
    {
        return Input.GetKey(keyCode);
    }

    public bool GetKeyDown()
    {
        return Input.GetKeyDown(keyCode);
    }

    public bool GetKeyUp()
    {
        return Input.GetKeyUp(keyCode);
    }
}
