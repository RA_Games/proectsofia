﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ScriptableKey))]
public class ScriptableKeyEditor : Editor
{
    ScriptableKey myTarget;

    private void OnEnable()
    {
        myTarget = (ScriptableKey)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.icon = (Sprite)EditorGUILayout.ObjectField("Icon", myTarget.icon, typeof(Sprite), false);
        myTarget.animationSheet = (AnimationSheet)EditorGUILayout.ObjectField("Animation", myTarget.animationSheet, typeof(AnimationSheet), false);
        myTarget.controllerType = (ControllerType)EditorGUILayout.EnumPopup(myTarget.controllerType);

        switch (myTarget.controllerType)
        {
            case ControllerType.PC:
                myTarget.pc_butons = (PcButtons)EditorGUILayout.EnumPopup(myTarget.pc_butons);

                if (myTarget.pc_butons == PcButtons.MOUSE)
                {
                    myTarget.mouse_buttons = (MouseButtons)EditorGUILayout.EnumPopup(myTarget.mouse_buttons);
                }
                else
                {
                    myTarget.keyCode = (KeyCode)EditorGUILayout.EnumPopup(myTarget.keyCode);
                }

                break;
            case ControllerType.XBOX:

                myTarget.xbox_buttons = (XboxButtons)EditorGUILayout.EnumPopup(myTarget.xbox_buttons);
                break;
            case ControllerType.PLAYSTATION:
                myTarget.play_buttons = (PlaystationButtons)EditorGUILayout.EnumPopup(myTarget.play_buttons);
                break;
            case ControllerType.SWITCH:
                myTarget.switch_buttons = (SwitchButtons)EditorGUILayout.EnumPopup(myTarget.switch_buttons);
                break;
        }
    }
}