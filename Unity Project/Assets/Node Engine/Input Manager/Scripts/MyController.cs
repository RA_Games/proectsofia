﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Project Sofia/My Control")]
public class MyController : ScriptableController
{
    [SerializeField]
    private ControllerType mode = ControllerType.PC;

    [SerializeField]
    private CrossPlatformAxis horizontal;
    [SerializeField]
    private CrossPlatformAxis vertical;

    [SerializeField]
    private CrossPlatformKeys submit;

    private ScriptableAxis horizontal_axis;
    private ScriptableAxis vertical_axis;
    private ScriptableKey submit_key;

    public void SetMode(ControllerType mode)
    {
        this.mode = mode;

        horizontal_axis = null;
        vertical_axis = null;
        submit_key = null;
        //And so..
    }

    public Vector2 GetAxis()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return new Vector3(horizontal_axis.GetAxis(), vertical_axis.GetAxis());
    }

    public Vector2 GetAxisRaw()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return new Vector3(horizontal_axis.GetAxisRaw(), vertical_axis.GetAxisRaw());
    }

    public ScriptableAxis GetHorizontalAxis()
    {
        if (horizontal_axis == null)
            horizontal_axis = horizontal.Get(mode);

        return horizontal_axis;
    }

    public ScriptableAxis GetVerticalAxis()
    {
        if (vertical_axis == null)
            vertical_axis = vertical.Get(mode);

        return vertical_axis;
    }

    public ScriptableKey Submit()
    {
        if (submit_key == null)
            submit_key = submit.GetKey(mode);

        return submit_key;
    }
}
