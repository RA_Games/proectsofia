﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/CrossPlatform Key")]
public class CrossPlatformKeys : ScriptableObject
{
    [SerializeField]
    private ControllerType mode;
    [SerializeField]
    private List<ScriptableKey> allowedKeys;

    public ScriptableKey GetKey()
    {
        foreach (ScriptableKey key in allowedKeys)
        {
            if (key.controllerType == mode)
                return key;
        }

        Debug.Log("Key not allowed for " + mode);
        return null;
    }

    public ScriptableKey GetKey(ControllerType mode)
    {
        foreach (ScriptableKey key in allowedKeys)
        {
            if (key.controllerType == mode)
                return key;
        }

        Debug.Log("Key not allowed for " + mode);
        return null;
    }
}
