﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Input Manager/Animation Sheet")]
public class AnimationSheet : ScriptableObject {

    public Sprite[] sprites;

    public float speed = 1f;
}
