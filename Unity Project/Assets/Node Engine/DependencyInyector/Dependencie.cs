﻿using UnityEngine;

public abstract class Dependencie : ScriptableObject
{
    public abstract T GetComponent<T>();
}
