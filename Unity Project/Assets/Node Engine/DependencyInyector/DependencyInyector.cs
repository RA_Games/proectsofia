﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Node Engine/Dependency Inyector/Main")]
public class DependencyInyector : ScriptableObject
{
    public List<Dependencie> dependencies = new List<Dependencie>();

    private static DependencyInyector _singleton;

    public static DependencyInyector Singleton
    {
        get
        {
            if (_singleton == null)
            {
                try
                {
                    _singleton = Resources.FindObjectsOfTypeAll<DependencyInyector>()[0];
                }
                catch
                {
                    Debug.LogError("No DependencyInjector has been found in resources!");
                }
            }

            return _singleton;
        }
    }

    public T GetDependency<T>() where T : Dependencie
    {
        for (int i = 0; i < dependencies.Count; i++)
        {
            if (dependencies[i] is T) {
                return (T)dependencies[i];
            }
        }

        return null;
    }
}
