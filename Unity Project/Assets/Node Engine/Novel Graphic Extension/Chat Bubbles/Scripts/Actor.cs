﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Node Engine/Novel/Actor")]
public class Actor : ScriptableObject {

    public string actor_name;

    [SerializeField]
    private Sprite[] images;

    public Sprite GetImage(Expresions expresion)
    {
        try
        {
            return images[(int)expresion];
        }
        catch
        {
            Debug.Log("Actor: " + actor_name + " Doesn't have image with expression '" + expresion +"'");
            return null;
        }
    }

    public enum Expresions
    {
        DEFAULT,ECSTASY,ANGRY,SAD,CRYING,ASHAMED,LAUGH,FEAR,BORING,UPSET,ROMANTIC,INTRIGUE,SURPRISE,ANXIETY,BETRAYAL,DISGUST,TERROR,DESPAIR
    }

}
