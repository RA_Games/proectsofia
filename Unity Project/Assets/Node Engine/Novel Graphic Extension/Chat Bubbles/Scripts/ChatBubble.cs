﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBubble : MonoBehaviour {

    public Actor actor;
    public MyController myController;
    public Vector2 offset;
    public Transform bubble_target;
    public Text textComponent;
    public Animator bubble_animator;
    public float speed = 0.1f;

    public bool autoSkip = true;
    private bool writingFinish;
    Action OnFinishRead;

    public static ChatBubble GetBubbleInScene(Actor actor)
    {
        foreach (ChatBubble bubble in FindObjectsOfType<ChatBubble>())
        {
            if (actor == bubble.actor)
                return bubble;
        }

        return null;
    }

    private void OnValidate()
    {
        Vector3 bubblePos = transform.position;
        bubblePos.x += offset.x;
        bubblePos.y = offset.y;
        bubble_target.position = bubblePos;
    }

    public void Write(string text, Action OnFinishRead)
    {
        this.OnFinishRead = OnFinishRead;
        StartCoroutine(WriteDialogue(text));
    }

    public void Write(string[] text, Action OnFinishRead)
    {
        this.OnFinishRead = OnFinishRead;
        StartCoroutine(WriteDialogues(text));
    }

    private void MoveBubbleToTarget()
    {
        Vector3 bubblePos = transform.position;
        bubblePos.x += offset.x;
        bubblePos.y = offset.y;
        bubble_target.position = bubblePos;

        Vector2 relativePos = transform.position - bubble_target.position;

        if (relativePos.x > 0 && relativePos.y > 0)
        {
            bubble_target.rotation = Quaternion.Euler(new Vector3(180, 0, 0));
        }
        else if (relativePos.x > 0 && relativePos.y < 0)
        {
            bubble_target.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
        else if (relativePos.x < 0 && relativePos.y < 0)
        {
            bubble_target.rotation = Quaternion.Euler(new Vector3(180, 180, 0));
        }
        else if (relativePos.x < 0 && relativePos.y > 0)
        {
            bubble_target.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }

    private IEnumerator WriteDialogues(string[] messages)
    {
        int index = 0;

        bubble_animator.SetBool("IsShowing", true);

        for (int i = 0; i < messages.Length; i++)
        {
            writingFinish = false;

            textComponent.text = string.Empty;

            bubble_animator.SetBool("IsWriting", true);

            yield return new WaitForSeconds(1);

            yield return StartCoroutine(TypeText(messages[index]));
            textComponent.text = messages[index];

            if (autoSkip)
            {
                yield return new WaitForSeconds(3);
                bubble_animator.SetBool("IsWriting", false);
                index++;
                continue;
            }
            else
            {
                bubble_animator.SetBool("IsWriting", false);

                while (!writingFinish)
                {
                    if (myController.Submit().GetKey())
                    {
                        writingFinish = true;
                        index++;
                        continue;
                    }

                    yield return new WaitForEndOfFrame();
                }


            }
        }

        bubble_animator.SetBool("IsShowing", false);
        yield return new WaitForSeconds(1);
        OnFinishRead.Invoke();
    }
    
    private IEnumerator WriteDialogue(string message)
    {
        writingFinish = false;

        textComponent.text = string.Empty;

        bubble_animator.SetBool("IsShowing", true);
        bubble_animator.SetBool("IsWriting", true);

        yield return new WaitForSeconds(1);

        yield return StartCoroutine(TypeText(message));

        textComponent.text = message;

        if (autoSkip)
        {
            yield return new WaitForSeconds(3);
            bubble_animator.SetBool("IsShowing", false);
            bubble_animator.SetBool("IsWriting", false);
            OnFinishRead.Invoke();
        }
        else
        {
            bubble_animator.SetBool("IsWriting", false);

            while (!writingFinish)
            {
                if (myController.Submit().GetKey())
                {
                    writingFinish = true;
                }

                yield return new WaitForEndOfFrame();
            }

            bubble_animator.SetBool("IsShowing", false);

            OnFinishRead.Invoke();
        }
    }

    private IEnumerator TypeText(string message)
    {
        foreach (char letter in message.ToCharArray())
        {
            textComponent.text += letter;
            yield return new WaitForSeconds(Time.deltaTime * speed);
        }
    }
    
}
